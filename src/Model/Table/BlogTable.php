<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Blog Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\Blog get($primaryKey, $options = [])
 * @method \App\Model\Entity\Blog newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Blog[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Blog|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Blog|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Blog patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Blog[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Blog findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class BlogTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('blog');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->addBehavior('Josegonzalez/Upload.Upload', [

            'foto1' => [
                'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                'fields' => [
                // if these fields or their defaults exist
                // the values will be set.
                'dir' => 'url_foto1', // defaults to `dir`
                
            ],
            'keepFilesOnDelete' => false,
           
            ],
            'foto2' => [
                'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                'fields' => [
                // if these fields or their defaults exist
                // the values will be set.
                'dir' => 'url_foto2', // defaults to `dir`
                
            ],
            'keepFilesOnDelete' => false,
           
            ],
            'foto3' => [
                'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                'fields' => [
                // if these fields or their defaults exist
                // the values will be set.
                'dir' => 'url_foto3', // defaults to `dir`
                
            ],
            'keepFilesOnDelete' => false,
           
            ],
            'foto4' => [
                'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                'fields' => [
                // if these fields or their defaults exist
                // the values will be set.
                'dir' => 'url_foto4', // defaults to `dir`
                
            ],
            'keepFilesOnDelete' => false,
           
            ],
            'foto5' => [
                'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                'fields' => [
                // if these fields or their defaults exist
                // the values will be set.
                'dir' => 'url_foto5', // defaults to `dir`
                
            ],
            'keepFilesOnDelete' => false,
           
            ],


        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('primer_titulo')
            ->maxLength('primer_titulo', 200)
            ->allowEmpty('primer_titulo');

        $validator
            ->scalar('segundo_titulo')
            ->maxLength('segundo_titulo', 200)
            ->allowEmpty('segundo_titulo');

        $validator
            ->scalar('introduccion')
            ->allowEmpty('introduccion');

        $validator
            ->scalar('primer_texto')
            ->allowEmpty('primer_texto');

        $validator
            ->scalar('segundo_texto')
            ->allowEmpty('segundo_texto');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));

        return $rules;
    }
}
