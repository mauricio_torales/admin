<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Colores Model
 *
 * @property \App\Model\Table\ProductosTable|\Cake\ORM\Association\BelongsToMany $Productos
 *
 * @method \App\Model\Entity\Colore get($primaryKey, $options = [])
 * @method \App\Model\Entity\Colore newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Colore[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Colore|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Colore|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Colore patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Colore[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Colore findOrCreate($search, callable $callback = null, $options = [])
 */
class ColoresTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('colores');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsToMany('Productos', [
            'foreignKey' => 'color_id',
            'targetForeignKey' => 'producto_id',
            'joinTable' => 'productos_colores'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('color')
            ->maxLength('color', 255)
            ->allowEmpty('color')
            ->add('color', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->scalar('codigo_color')
            ->maxLength('codigo_color', 255)
            ->allowEmpty('codigo_color');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['color']));

        return $rules;
    }
}
