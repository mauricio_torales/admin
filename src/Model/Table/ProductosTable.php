<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Productos Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\MarcasTable|\Cake\ORM\Association\BelongsTo $Marcas
 * @property \App\Model\Table\CategoriasTable|\Cake\ORM\Association\BelongsTo $Categorias
 * @property \App\Model\Table\ColoresTable|\Cake\ORM\Association\BelongsToMany $Colores
 * @property \App\Model\Table\TallesTable|\Cake\ORM\Association\BelongsToMany $Talles
 *
 * @method \App\Model\Entity\Producto get($primaryKey, $options = [])
 * @method \App\Model\Entity\Producto newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Producto[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Producto|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Producto|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Producto patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Producto[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Producto findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class ProductosTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('productos');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Users', [
            'foreignKey' => 'user_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Marcas', [
            'foreignKey' => 'marca_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Categorias', [
            'foreignKey' => 'categoria_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsToMany('Colores', [
            'foreignKey' => 'producto_id',
            'targetForeignKey' => 'color_id',
            'joinTable' => 'productos_colores'
        ]);
        $this->belongsToMany('Talles', [
            'foreignKey' => 'producto_id',
            'targetForeignKey' => 'talle_id',
            'joinTable' => 'productos_talles'
        ]);
        $this->addBehavior('Josegonzalez/Upload.Upload', [

                'foto1' => [
                    'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                    'fields' => [
                    // if these fields or their defaults exist
                    // the values will be set.
                    'dir' => 'foto1_dir', // defaults to `dir`
                    
                ],
                'keepFilesOnDelete' => false,
               
                ],
                'foto2' => [
                    'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                    'fields' => [
                    // if these fields or their defaults exist
                    // the values will be set.
                    'dir' => 'foto2_dir', // defaults to `dir`
                    
                ],
                'keepFilesOnDelete' => false,
               
                ],
                'foto3' => [
                    'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                    'fields' => [
                    // if these fields or their defaults exist
                    // the values will be set.
                    'dir' => 'foto3_dir', // defaults to `dir`
                    
                ],
                'keepFilesOnDelete' => false,
               
                ],
                'foto4' => [
                    'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                    'fields' => [
                    // if these fields or their defaults exist
                    // the values will be set.
                    'dir' => 'foto4_dir', // defaults to `dir`
                    
                ],
                'keepFilesOnDelete' => false,
               
                ],
                'foto5' => [
                    'path' => 'webroot{DS}img{DS}{field}{DS}{microtime}{DS}',
                    'fields' => [
                    // if these fields or their defaults exist
                    // the values will be set.
                    'dir' => 'foto5_dir', // defaults to `dir`
                    
                ],
                'keepFilesOnDelete' => false,
               
                ],


            ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('nombre')
            ->maxLength('nombre', 250)
            ->allowEmpty('nombre');

        $validator
            ->scalar('descripcion')
            ->allowEmpty('descripcion');

        $validator
            ->scalar('destacado')
            ->maxLength('destacado', 250)
            ->allowEmpty('destacado');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['user_id'], 'Users'));
        $rules->add($rules->existsIn(['marca_id'], 'Marcas'));
        $rules->add($rules->existsIn(['categoria_id'], 'Categorias'));

        return $rules;
    }
}
