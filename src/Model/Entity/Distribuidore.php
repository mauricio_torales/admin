<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Distribuidore Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $nombre
 * @property string|null $telefono
 * @property string|null $horario
 * @property string|null $direccion
 * @property string|null $localidad
 * @property string|null $latitud
 * @property string|null $longitud
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\User $user
 */
class Distribuidore extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'nombre' => true,
        'telefono' => true,
        'horario' => true,
        'direccion' => true,
        'localidad' => true,
        'latitud' => true,
        'longitud' => true,
        'created' => true,
        'modified' => true,
        'user' => true
    ];
}
