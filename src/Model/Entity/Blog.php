<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Blog Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $primer_titulo
 * @property string|null $segundo_titulo
 * @property string|null $introduccion
 * @property string|null $primer_texto
 * @property string|null $segundo_texto
 * @property string|null $url_foto1
 * @property string|null $url_foto2
 * @property string|null $url_foto3
 * @property string|null $url_foto4
 * @property string|null $url_foto5
 * @property string|null $foto1
 * @property string|null $foto2
 * @property string|null $foto3
 * @property string|null $foto4
 * @property string|null $foto5
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\User $user
 */
class Blog extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'primer_titulo' => true,
        'segundo_titulo' => true,
        'introduccion' => true,
        'primer_texto' => true,
        'segundo_texto' => true,
        'url_foto1' => true,
        'url_foto2' => true,
        'url_foto3' => true,
        'url_foto4' => true,
        'url_foto5' => true,
        'foto1' => true,
        'foto2' => true,
        'foto3' => true,
        'foto4' => true,
        'foto5' => true,
        'created' => true,
        'modified' => true,
        'user' => true
    ];
}
