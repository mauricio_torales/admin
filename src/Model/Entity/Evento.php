<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Evento Entity
 *
 * @property int $id
 * @property int $user_id
 * @property string|null $titulo
 * @property string|null $descripcion
 * @property string|null $fecha
 * @property string|null $hora
 * @property string|null $lugar
 * @property string|null $url_foto1
 * @property string|null $foto1
 * @property string|null $url_foto2
 * @property string|null $foto2
 * @property string|null $url_foto3
 * @property string|null $fotoo3
 * @property \Cake\I18n\FrozenTime|null $created
 * @property \Cake\I18n\FrozenTime|null $modified
 *
 * @property \App\Model\Entity\User $user
 */
class Evento extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_id' => true,
        'titulo' => true,
        'descripcion' => true,
        'fecha' => true,
        'hora' => true,
        'lugar' => true,
        'url_foto1' => true,
        'foto1' => true,
        'url_foto2' => true,
        'foto2' => true,
        'url_foto3' => true,
        'fotoo3' => true,
        'created' => true,
        'modified' => true,
        'user' => true
    ];
}
