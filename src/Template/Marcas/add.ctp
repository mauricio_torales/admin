<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Marca $marca
 */
?>
<header class="page-header">
    <h2>Agregar marca</h2>   
</header>

<div class="row">
                            <div class="col-lg-12">
                                <section class="panel">
                                    <header class="panel-heading">
                                       
                        
                                        <h2 class="panel-title"></h2>
                                    </header>
                                    <div class="panel-body">

    <?= $this->Form->create($marca, ['type' => 'file']) ?>
  
                                                <div class="form-group">
                                                <label class="col-md-3 control-label" for="inputDefault">Nombre de la marca</label>
                                                <div class="col-md-6">

                                                    <input type="text" class="form-control" name="nombre_marca" id="nombre_marca">

                                                </div>
                                            </div>
                                             <div class="form-group">
                                                <label class="col-md-3 control-label" for="inputDefault">Logo de la marca</label>
                                                <div class="col-md-6">
                                                <input type="file" class="form-control-file" name="foto_marca" id="foto_marca">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-3 control-label" for="inputDefault">Banner de la marca</label>
                                                <div class="col-md-6">
                                                <input type="file" class="form-control-file" name="banner_marca" id="banner_marca">
                                                </div>
                                            </div>


      
          
    
  
    <br>
                                            <center>
                                            <button type="Submit" class="mb-xs mt-xs mr-xs btn btn-default"><i class="fa  fa-save"></i> Guardar</button>
                                            </center>
    <?= $this->Form->end() ?>

</div>
