<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Evento[]|\Cake\Collection\CollectionInterface $eventos
 */
?>
<header class="page-header">
    <h2>Eventos</h2>  
</header>
<section class="panel">
              <header class="panel-heading">
                <div class="panel-actions">

                  <button type="button" onclick="window.location.href='eventos/add/'" class="mb-xs mt-xs mr-xs btn btn-success"><i class="fa fa-plus-circle"></i> Agregar evento</button>
                 
                </div>
            
                <h2 class="panel-title">Lista de eventos</h2>
              </header>
              <div class="panel-body">
                <table class="table table-bordered table-striped mb-none" id="datatable-default">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Nombre</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
            <?php foreach ($eventos as $evento): ?>
            <tr class="gradeA">
                <td><?= $this->Number->format($evento->id) ?></td>
                <td><?= h($evento->titulo) ?></td>
                <td class="actions">
                                                            
                  <?= $this->Html->link($this->Html->tag('i', ' editar', array('class' => 'fa fa-pencil')), ['action' => 'edit', $evento->id], array('escape' => false)) ?>
                  <?= $this->Form->postLink($this->Html->tag('i', ' eliminar', array('class' => 'fa fa-trash-o')), ['action' => 'delete', $evento->id],array('escape' => false), ['confirm' => __('Esta seguro de eliminar este producto?', $evento->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
         </tbody>
                </table>
              
              </div>



</section>