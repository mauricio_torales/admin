<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Evento $evento
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Evento'), ['action' => 'edit', $evento->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Evento'), ['action' => 'delete', $evento->id], ['confirm' => __('Are you sure you want to delete # {0}?', $evento->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Eventos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Evento'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="eventos view large-9 medium-8 columns content">
    <h3><?= h($evento->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $evento->has('user') ? $this->Html->link($evento->user->id, ['controller' => 'Users', 'action' => 'view', $evento->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Titulo') ?></th>
            <td><?= h($evento->titulo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Fecha') ?></th>
            <td><?= h($evento->fecha) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Hora') ?></th>
            <td><?= h($evento->hora) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Lugar') ?></th>
            <td><?= h($evento->lugar) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($evento->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($evento->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($evento->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descripcion') ?></h4>
        <?= $this->Text->autoParagraph(h($evento->descripcion)); ?>
    </div>
    <div class="row">
        <h4><?= __('Url Foto1') ?></h4>
        <?= $this->Text->autoParagraph(h($evento->url_foto1)); ?>
    </div>
    <div class="row">
        <h4><?= __('Foto1') ?></h4>
        <?= $this->Text->autoParagraph(h($evento->foto1)); ?>
    </div>
    <div class="row">
        <h4><?= __('Url Foto2') ?></h4>
        <?= $this->Text->autoParagraph(h($evento->url_foto2)); ?>
    </div>
    <div class="row">
        <h4><?= __('Foto2') ?></h4>
        <?= $this->Text->autoParagraph(h($evento->foto2)); ?>
    </div>
    <div class="row">
        <h4><?= __('Url Foto3') ?></h4>
        <?= $this->Text->autoParagraph(h($evento->url_foto3)); ?>
    </div>
    <div class="row">
        <h4><?= __('Fotoo3') ?></h4>
        <?= $this->Text->autoParagraph(h($evento->fotoo3)); ?>
    </div>
</div>
