<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Producto[]|\Cake\Collection\CollectionInterface $productos
 */
?>
<header class="page-header">
    <h2>Productos</h2>  
</header>
   <section class="panel">
              <header class="panel-heading">
                <div class="panel-actions">

                  <button type="button" onclick="window.location.href='productos/add/'" class="mb-xs mt-xs mr-xs btn btn-success"><i class="fa fa-plus-circle"></i> Agregar producto</button>
                 
                </div>
            
                <h2 class="panel-title">Lista de productos</h2>
              </header>
              <div class="panel-body">
                <table class="table table-bordered table-striped mb-none" id="datatable-default">
                  <thead>
                    <tr>
                      <th>Id</th>

                      <th>Codigo</th>
                      <th>Nombre</th>
                      <th>imagen</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
            <?php foreach ($productos as $producto): ?>
            <tr class="gradeA">
                <td><?= $this->Number->format($producto->id) ?></td>
                 <td><?= h($producto->codigo) ?></td>
                <td><?= h($producto->nombre) ?></td>
                <td>
                  <?php
                  echo'<img style="width:100px;height:100px;" src="http://cyclesport.com.py/admin-cycles/app/'.$producto->foto1_dir.''.$producto->foto1.'">';
                  
                  ?>
                </td>
                <td class="actions">
                                                            
                  <?= $this->Html->link($this->Html->tag('i', ' editar', array('class' => 'fa fa-pencil')), ['action' => 'edit', $producto->id], array('escape' => false)) ?>
                  <?= $this->Form->postLink($this->Html->tag('i', ' eliminar', array('class' => 'fa fa-trash-o')), ['action' => 'delete', $producto->id],array('escape' => false), ['confirm' => __('Esta seguro de eliminar este producto?', $producto->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
         </tbody>
                </table>
              
              </div>



            </section>

