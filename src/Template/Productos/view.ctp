<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Producto $producto
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Producto'), ['action' => 'edit', $producto->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Producto'), ['action' => 'delete', $producto->id], ['confirm' => __('Are you sure you want to delete # {0}?', $producto->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Productos'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Producto'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Marcas'), ['controller' => 'Marcas', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Marca'), ['controller' => 'Marcas', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Categorias'), ['controller' => 'Categorias', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Categoria'), ['controller' => 'Categorias', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Colores'), ['controller' => 'Colores', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Colore'), ['controller' => 'Colores', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Talles'), ['controller' => 'Talles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Talle'), ['controller' => 'Talles', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="productos view large-9 medium-8 columns content">
    <h3><?= h($producto->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $producto->has('user') ? $this->Html->link($producto->user->id, ['controller' => 'Users', 'action' => 'view', $producto->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Marca') ?></th>
            <td><?= $producto->has('marca') ? $this->Html->link($producto->marca->id, ['controller' => 'Marcas', 'action' => 'view', $producto->marca->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Categoria') ?></th>
            <td><?= $producto->has('categoria') ? $this->Html->link($producto->categoria->id, ['controller' => 'Categorias', 'action' => 'view', $producto->categoria->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nombre') ?></th>
            <td><?= h($producto->nombre) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Destacado') ?></th>
            <td><?= h($producto->destacado) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($producto->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($producto->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($producto->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Descripcion') ?></h4>
        <?= $this->Text->autoParagraph(h($producto->descripcion)); ?>
    </div>
    <div class="row">
        <h4><?= __('Foto1 Dir') ?></h4>
        <?= $this->Text->autoParagraph(h($producto->foto1_dir)); ?>
    </div>
    <div class="row">
        <h4><?= __('Foto1') ?></h4>
        <?= $this->Text->autoParagraph(h($producto->foto1)); ?>
    </div>
    <div class="row">
        <h4><?= __('Foto2 Dir') ?></h4>
        <?= $this->Text->autoParagraph(h($producto->foto2_dir)); ?>
    </div>
    <div class="row">
        <h4><?= __('Foto2') ?></h4>
        <?= $this->Text->autoParagraph(h($producto->foto2)); ?>
    </div>
    <div class="row">
        <h4><?= __('Foto3 Dir') ?></h4>
        <?= $this->Text->autoParagraph(h($producto->foto3_dir)); ?>
    </div>
    <div class="row">
        <h4><?= __('Foto3') ?></h4>
        <?= $this->Text->autoParagraph(h($producto->foto3)); ?>
    </div>
    <div class="row">
        <h4><?= __('Foto4 Dir') ?></h4>
        <?= $this->Text->autoParagraph(h($producto->foto4_dir)); ?>
    </div>
    <div class="row">
        <h4><?= __('Foto4') ?></h4>
        <?= $this->Text->autoParagraph(h($producto->foto4)); ?>
    </div>
    <div class="row">
        <h4><?= __('Foto5 Dir') ?></h4>
        <?= $this->Text->autoParagraph(h($producto->foto5_dir)); ?>
    </div>
    <div class="row">
        <h4><?= __('Foto5') ?></h4>
        <?= $this->Text->autoParagraph(h($producto->foto5)); ?>
    </div>
    <div class="related">
        <h4><?= __('Related Colores') ?></h4>
        <?php if (!empty($producto->colores)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Color') ?></th>
                <th scope="col"><?= __('Codigo Color') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($producto->colores as $colores): ?>
            <tr>
                <td><?= h($colores->id) ?></td>
                <td><?= h($colores->color) ?></td>
                <td><?= h($colores->codigo_color) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Colores', 'action' => 'view', $colores->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Colores', 'action' => 'edit', $colores->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Colores', 'action' => 'delete', $colores->id], ['confirm' => __('Are you sure you want to delete # {0}?', $colores->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related Talles') ?></h4>
        <?php if (!empty($producto->talles)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Talle') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($producto->talles as $talles): ?>
            <tr>
                <td><?= h($talles->id) ?></td>
                <td><?= h($talles->talle) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Talles', 'action' => 'view', $talles->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Talles', 'action' => 'edit', $talles->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Talles', 'action' => 'delete', $talles->id], ['confirm' => __('Are you sure you want to delete # {0}?', $talles->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
