<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Banner[]|\Cake\Collection\CollectionInterface $banners
 */
?>

<header class="page-header">
    <h2>banners</h2>  
</header>
<section class="panel">
              <header class="panel-heading">
                <div class="panel-actions">

                  <button type="button" onclick="window.location.href='banners/add/'" class="mb-xs mt-xs mr-xs btn btn-success"><i class="fa fa-plus-circle"></i> Agregar banner</button>
                 
                </div>
            
                <h2 class="panel-title">Lista de banners</h2>
              </header>
              <div class="panel-body">
                <table class="table table-bordered table-striped mb-none" id="datatable-default">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Seccion</th>
                      <th>Imagen</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
            <?php foreach ($banners as $banner): ?>
            <tr class="gradeA">
                <td><?= $this->Number->format($banner->id) ?></td>
                <td><?= h($banner->seccion) ?></td>
                <td>
                  <?php
                  echo'<img style="width:100px;" src="'.$banner->foto_dir.''.$banner->foto.'">';
                  ?>
                </td>
                <td class="actions">                                        
                  <?= $this->Html->link($this->Html->tag('i', ' editar', array('class' => 'fa fa-pencil')), ['action' => 'edit', $banner->id], array('escape' => false)) ?>
                  <?= $this->Form->postLink($this->Html->tag('i', ' eliminar', array('class' => 'fa fa-trash-o')), ['action' => 'delete', $banner->id],array('escape' => false), ['confirm' => __('Esta seguro de eliminar este producto?', $banner->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
         </tbody>
                </table>
              
              </div>



</section>
