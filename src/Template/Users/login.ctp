
<section class="body-sign">

			<div class="center-sign">
			
				<div class="panel panel-sign">
				<?= $this->Form->create() ?>	
				<div class="panel-body" style="border-top-width: 0px;">
						<?= $this->Html->image('logo-negro.png', ['alt' => 'CYCLES']); ?>
						
							<div class="form-group mb-lg">
								<label>Correo</label>
								<div class="input-group input-group-icon">
									<input name="email" id="email" type="email" class="form-control input-lg" required/>
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-user"></i>
										</span>
									</span>
								</div>
							</div>

							<div class="form-group mb-lg">
								<div class="clearfix">
									<label class="pull-left">Contraseña</label>
								</div>
								<div class="input-group input-group-icon">
									<input name="password" id="password" type="password" class="form-control input-lg" required />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
								</div>
							</div>

							
						
							<div class="mb-xs text-center">
								<button class="btn btn-facebook mb-md ml-xs mr-xs" type="submit">Ingresar</button>
							</div>

						<?= $this->Form->end() ?>
					</div>
				</div>
				<?= $this->Flash->render() ?>
			</div>
			
		</section>