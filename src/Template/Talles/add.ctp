<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Talle $talle
 */
?>
<header class="page-header">
    <h2>Agregar tamaño</h2>   
</header>
<div class="row">
                            <div class="col-lg-12">
                                <section class="panel">
                                    <header class="panel-heading">
                                       
                        
                                        <h2 class="panel-title"></h2>
                                    </header>
                                    <div class="panel-body">
    <?= $this->Form->create($talle) ?>
    
       <div class="form-group">
                                                <label class="col-md-3 control-label" for="inputDefault">Abreviatura del tamaño</label>
                                                <div class="col-md-6">

                                                    <input type="text" class="form-control" name="talle" id="talle">

                                                </div>
                                            </div>
   
    <br>
                                            <center>
                                            <button type="Submit" class="mb-xs mt-xs mr-xs btn btn-default"><i class="fa  fa-save"></i> Guardar</button>
                                            </center>
    <?= $this->Form->end() ?>
</div>
