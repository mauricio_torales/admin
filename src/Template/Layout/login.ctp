<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Cyclesport';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>
        <?= $cakeDescription ?>
    </title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('vendor/bootstrap/css/bootstrap.css') ?>
    <?= $this->Html->css('vendor/font-awesome/css/font-awesome.css') ?>
    
    <?= $this->Html->css('theme.css') ?>
    <?= $this->Html->css('theme-custom.css') ?>
    <?= $this->Html->css('theme-admin-extension.css') ?>

   
    <?= $this->Html->script('vendor/modernizr/modernizr.js') ?>

    

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

</head>
<body style="background-image: url(http://cyclesport.com.py/admin-cycles/app/webroot/img/slider.jpg); background-size: cover;">
    
   
    <div class="container clearfix">
        
        <?= $this->fetch('content') ?>

    </div>

    
<footer>
</footer>
<?= $this->Html->script('vendor/jquery/jquery.js') ?>
<?= $this->Html->script('vendor/jquery-browser-mobile/jquery.browser.mobile.js') ?>
<?= $this->Html->script('vendor/bootstrap/js/bootstrap.js') ?>

</body>
</html>
