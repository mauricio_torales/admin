<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Distribuidore[]|\Cake\Collection\CollectionInterface $distribuidores
 */
?>
<header class="page-header">
    <h2> Distribuidores</h2>   
</header>
<section class="panel">
              <header class="panel-heading">
                <div class="panel-actions">

                  <button type="button" onclick="window.location.href='distribuidores/add/'" class="mb-xs mt-xs mr-xs btn btn-success"><i class="fa fa-plus-circle"></i> Agregar distribuidor</button>
                 
                </div>
            
                <h2 class="panel-title">Lista de distribuidores</h2>
              </header>
              <div class="panel-body">
                <table class="table table-bordered table-striped mb-none" id="datatable-default">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Sucursal</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
            <?php foreach ($distribuidores as $distribuidor): ?>
            <tr class="gradeA">
                <td><?= $this->Number->format($distribuidor->id) ?></td>
                <td><?= h($distribuidor->nombre) ?></td>
                <td class="actions">
                                                            
                  <?= $this->Html->link($this->Html->tag('i', ' editar', array('class' => 'fa fa-pencil')), ['action' => 'edit', $distribuidor->id], array('escape' => false)) ?>
                  <?= $this->Form->postLink($this->Html->tag('i', ' eliminar', array('class' => 'fa fa-trash-o')), ['action' => 'delete', $distribuidor->id],array('escape' => false), ['confirm' => __('Esta seguro de eliminar este distribuidor?', $distribuidor->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
         </tbody>
                </table>
              
              </div>



</section>