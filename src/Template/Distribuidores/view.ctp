<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Distribuidore $distribuidore
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Distribuidore'), ['action' => 'edit', $distribuidore->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Distribuidore'), ['action' => 'delete', $distribuidore->id], ['confirm' => __('Are you sure you want to delete # {0}?', $distribuidore->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Distribuidores'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Distribuidore'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="distribuidores view large-9 medium-8 columns content">
    <h3><?= h($distribuidore->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $distribuidore->has('user') ? $this->Html->link($distribuidore->user->id, ['controller' => 'Users', 'action' => 'view', $distribuidore->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Nombre') ?></th>
            <td><?= h($distribuidore->nombre) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Telefono') ?></th>
            <td><?= h($distribuidore->telefono) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Horario') ?></th>
            <td><?= h($distribuidore->horario) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Direccion') ?></th>
            <td><?= h($distribuidore->direccion) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Localidad') ?></th>
            <td><?= h($distribuidore->localidad) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Latitud') ?></th>
            <td><?= h($distribuidore->latitud) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Longitud') ?></th>
            <td><?= h($distribuidore->longitud) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($distribuidore->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($distribuidore->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($distribuidore->modified) ?></td>
        </tr>
    </table>
</div>
