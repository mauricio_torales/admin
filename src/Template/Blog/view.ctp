<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Blog $blog
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Blog'), ['action' => 'edit', $blog->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Blog'), ['action' => 'delete', $blog->id], ['confirm' => __('Are you sure you want to delete # {0}?', $blog->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Blog'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Blog'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="blog view large-9 medium-8 columns content">
    <h3><?= h($blog->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $blog->has('user') ? $this->Html->link($blog->user->id, ['controller' => 'Users', 'action' => 'view', $blog->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Primer Titulo') ?></th>
            <td><?= h($blog->primer_titulo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Segundo Titulo') ?></th>
            <td><?= h($blog->segundo_titulo) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($blog->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($blog->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($blog->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Introduccion') ?></h4>
        <?= $this->Text->autoParagraph(h($blog->introduccion)); ?>
    </div>
    <div class="row">
        <h4><?= __('Primer Texto') ?></h4>
        <?= $this->Text->autoParagraph(h($blog->primer_texto)); ?>
    </div>
    <div class="row">
        <h4><?= __('Segundo Texto') ?></h4>
        <?= $this->Text->autoParagraph(h($blog->segundo_texto)); ?>
    </div>
    <div class="row">
        <h4><?= __('Url Foto1') ?></h4>
        <?= $this->Text->autoParagraph(h($blog->url_foto1)); ?>
    </div>
    <div class="row">
        <h4><?= __('Url Foto2') ?></h4>
        <?= $this->Text->autoParagraph(h($blog->url_foto2)); ?>
    </div>
    <div class="row">
        <h4><?= __('Url Foto3') ?></h4>
        <?= $this->Text->autoParagraph(h($blog->url_foto3)); ?>
    </div>
    <div class="row">
        <h4><?= __('Url Foto4') ?></h4>
        <?= $this->Text->autoParagraph(h($blog->url_foto4)); ?>
    </div>
    <div class="row">
        <h4><?= __('Url Foto5') ?></h4>
        <?= $this->Text->autoParagraph(h($blog->url_foto5)); ?>
    </div>
    <div class="row">
        <h4><?= __('Foto1') ?></h4>
        <?= $this->Text->autoParagraph(h($blog->foto1)); ?>
    </div>
    <div class="row">
        <h4><?= __('Foto2') ?></h4>
        <?= $this->Text->autoParagraph(h($blog->foto2)); ?>
    </div>
    <div class="row">
        <h4><?= __('Foto3') ?></h4>
        <?= $this->Text->autoParagraph(h($blog->foto3)); ?>
    </div>
    <div class="row">
        <h4><?= __('Foto4') ?></h4>
        <?= $this->Text->autoParagraph(h($blog->foto4)); ?>
    </div>
    <div class="row">
        <h4><?= __('Foto5') ?></h4>
        <?= $this->Text->autoParagraph(h($blog->foto5)); ?>
    </div>
</div>
