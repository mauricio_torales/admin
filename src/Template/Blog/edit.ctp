<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Blog $blog
 */
?>
<style>
  @import url(https://fonts.googleapis.com/icon?family=Material+Icons);
  @import url("https://fonts.googleapis.com/css?family=Raleway");
  .box {
    display: block;
    min-width: 300px;
    height: 300px;
    margin: 10px;
    background-color: white;
    border-radius: 5px;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
    transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
    overflow: hidden;
  }
  .box-2 {
    display: block;
    min-width: 300px;
    height: 300px;
    margin: 10px;
    background-color: white;
    border-radius: 5px;
    box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);
    transition: all 0.3s cubic-bezier(0.25, 0.8, 0.25, 1);
    overflow: hidden;
  }
  .upload-options {
    position: relative;
    height: 75px;
    background-color: cadetblue;
    cursor: pointer;
    overflow: hidden;
    text-align: center;
    transition: background-color ease-in-out 150ms;
  }
  .upload-options:hover {
    background-color: #7fb1b3;
  }
  .upload-options input {
    width: 0.1px;
    height: 0.1px;
    opacity: 0;
    overflow: hidden;
    position: absolute;
    z-index: -1;
  }
  .upload-options label {
    display: flex;
    align-items: center;
    width: 100%;
    height: 100%;
    font-weight: 400;
    text-overflow: ellipsis;
    white-space: nowrap;
    cursor: pointer;
    overflow: hidden;
  }
  .upload-options label::after {
    content: 'add';
    font-family: 'Material Icons';
    position: absolute;
    font-size: 2.5rem;
    color: #e6e6e6;
    top: calc(50% - 2.5rem);
    left: calc(50% - 1.25rem);
    z-index: 0;
  }
  .upload-options label span {
    display: inline-block;
    width: 50%;
    height: 100%;
    text-overflow: ellipsis;
    white-space: nowrap;
    overflow: hidden;
    vertical-align: middle;
    text-align: center;
  }
  .upload-options label span:hover i.material-icons {
    color: lightgray;
  }

  .js--image-preview {
    height: 225px;
    width: 100%;
    position: relative;
    overflow: hidden;
    background-image: url("");
    background-color: white;
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
  }
  .js--image-preview::after {
   
    position: relative;
    font-size: 4.5em;
    color: #e6e6e6;
    top: calc(50% - 3rem);
    left: calc(50% - 2.25rem);
    z-index: 0;
  }
  .js--image-preview.js--no-default::after {
    display: none!important;
  }
  .js--image-preview:nth-child(2) {
    background-image: url("http://bastianandre.at/giphy.gif");
  }

  i.material-icons {
    transition: color 100ms ease-in-out;
    font-size: 2.25em;
    line-height: 55px;
    color: white;
    display: block;
  }

  .drop {
    display: block;
    position: absolute;
    background: rgba(95, 158, 160, 0.2);
    border-radius: 100%;
    -webkit-transform: scale(0);
            transform: scale(0);
  }

  .animate {
    -webkit-animation: ripple 0.4s linear;
            animation: ripple 0.4s linear;
  }
  input::placeholder, textarea::placeholder{
    color:#151b1e40!important;
  }
  .jqte_tool_label{
      height:25px!important;
  }
  @-webkit-keyframes ripple {
    100% {
      opacity: 0;
      -webkit-transform: scale(2.5);
              transform: scale(2.5);
    }
  }

  @keyframes ripple {
    100% {
      opacity: 0;
      -webkit-transform: scale(2.5);
              transform: scale(2.5);
    }
  }

</style>
<header class="page-header">
    <h2>Modificar información del blog</h2>   
</header>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <div class="row">
                            <?= $this->Form->create($blog, ['type' => 'file','id'=>'formulario']) ?>
                                <div class="col-sm-4">
                                    <center><label for="name">Foto slider</label></center>
                                    <div class="box">
                                            <?php 
                                            if (!empty($blog->foto1)) {
                                              echo '<div class="js--image-preview"><img id="img-foto1" style="height:100%; display:block; margin: auto!important;" src="http://localhost/cyclesport/admin/'.$blog->url_foto1.''.$blog->foto1.'"></div>';

                                            }else{
                                              echo '<div class="js--image-preview"></div>';
                                            }
                                            ?>
                                        <div class="upload-options">
                                        <label class="lbl-foto1">
                                            <input type="file" id="foto1" name="" class="image-upload" accept="image/*" />
                                        </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <center><label for="name">Foto Slider</label></center>
                                    <div class="box">
                                            <?php 
                                            if (!empty($blog->foto2)) {
                                              echo '<div class="js--image-preview"><img id="img-foto2" style="height:100%; display:block; margin: auto!important;" src="http://localhost/cyclesport/admin/'.$blog->url_foto2.''.$blog->foto2.'"></div>';

                                            }else{
                                              echo '<div class="js--image-preview"></div>';
                                            }
                                            ?>
                                        <div class="upload-options">
                                        <label class="lbl-foto2">
                                            <input type="file" id="foto2" name="" class="image-upload" accept="image/*" />
                                        </label>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-sm-4">
                                    <center><label for="name">Foto Slider</label></center>
                                    <div class="box">
                                            <?php 
                                            if (!empty($blog->foto3)) {
                                              echo '<div class="js--image-preview"><img id="img-foto3" style="height:100%; display:block; margin: auto!important;" src="http://localhost/cyclesport/admin/'.$blog->url_foto3.''.$blog->foto3.'"></div>';

                                            }else{
                                              echo '<div class="js--image-preview"></div>';
                                            }
                                            ?>
                                        <div class="upload-options">
                                        <label class="lbl-foto3">
                                            <input type="file" id="foto3" name="" class="image-upload" accept="image/*" />
                                        </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
							<div class="col-xs-12">
								<section class="panel">
									<header class="panel-heading">
										<h2 class="panel-title">Texto principal</h2>
									</header>
									<div class="panel-body">
											<div class="form-group">
												<div class="col-md-12">
                                                  <?php echo $this->Form->control('introduccion',['class'=>'summernote','id'=>'texto','data-plugin-summernote data-plugin-options'=>'{ "height": 180, "codemirror": { "theme": "ambiance" } }']);?>
												</div>
											</div>
									</div>
								</section>
                            </div>
                            <div class="col-xs-12">
								<section class="panel">
									<header class="panel-heading">
										<h2 class="panel-title">Primer artículo</h2>
									</header>
									<div class="panel-body">
											<div class="form-group">
                                                <div class="col-md-6">
                                                    <div class="box">
                                                            <?php 
                                                            if (!empty($blog->foto4)) {
                                                            echo '<div class="js--image-preview"><img id="img-foto4" style="height:100%; display:block; margin: auto!important;" src="http://localhost/cyclesport/admin/'.$blog->url_foto4.''.$blog->foto4.'"></div>';

                                                            }else{
                                                            echo '<div class="js--image-preview"></div>';
                                                            }
                                                            ?>
                                                        <div class="upload-options">
                                                        <label class="lbl-foto4">
                                                            <input type="file" id="foto4" name="" class="image-upload" accept="image/*" />
                                                        </label>
                                                        </div>
                                                    </div>
                                                </div>
												<div class="col-md-6">
                                                           <?php echo $this->Form->control('primer_titulo',['label' => false,'class'=>'form-control',]);?>
                                                            <?php echo $this->Form->control('primer_texto',['label' => false,'id'=>'texto2','class'=>'summernote','data-plugin-summernote data-plugin-options'=>'{ "height": 180, "codemirror": { "theme": "ambiance" } }']);?>
												</div>
											</div>
									</div>
								</section>
                            </div>
                            <div class="col-xs-12">
								<section class="panel">
									<header class="panel-heading">
										<h2 class="panel-title">Segundo artículo</h2>
									</header>
									<div class="panel-body">
											<div class="form-group">
												<div class="col-md-6">
                                                    <?php echo $this->Form->control('segundo_titulo',['label' => false,'class'=>'form-control',]);?> 
                                                    <?php echo $this->Form->control('segundo_texto',['label' => false,'id'=>'texto3','class'=>'summernote','data-plugin-summernote data-plugin-options'=>'{ "height": 180, "codemirror": { "theme": "ambiance" } }']);?>
                                                </div>
                                                <div class="col-md-6">
                                                    <div class="box">
                                                            <?php 
                                                            if (!empty($blog->foto5)) {
                                                            echo '<div class="js--image-preview"><img id="img-foto5" style="height:100%; display:block; margin: auto!important;" src="http://localhost/cyclesport/admin/'.$blog->url_foto5.''.$blog->foto5.'"></div>';

                                                            }else{
                                                            echo '<div class="js--image-preview"></div>';
                                                            }
                                                            ?>
                                                        <div class="upload-options">
                                                        <label class="lbl-foto5">
                                                            <input type="file" id="foto5" name="" class="image-upload" accept="image/*" />
                                                        </label>
                                                        </div>
                                                    </div>
                                                </div>
											</div>
									</div>
								</section>
                            </div>
             <button id="guardar" type="Submit" class="btn btn-lg  btn-block"><i class="fa fa-save"></i> Guardar</button>
             <?= $this->Form->end() ?> 
            </div>
        </section>
    </div>
</div>
<script>
       $(document).on("submit","#formulario",function(e){
            $("#texto").val($('#texto').code());
            $("#texto2").val($('#texto2').code());
            $("#texto3").val($('#texto3').code());
            return true;
        });
       
     $('.lbl-foto1').click(function(){
            $('#foto1').attr('name', 'foto1');
            $('#img-foto1').attr('style','display:none;')
        });
    $('.lbl-foto2').click(function(){
            $('#foto2').attr('name', 'foto2');
            $('#img-foto2').attr('style','display:none;')
        });
    $('.lbl-foto3').click(function(){
            $('#foto3').attr('name', 'foto3');
            $('#img-foto3').attr('style','display:none;')
        });
    $('.lbl-foto4').click(function(){
            $('#foto4').attr('name', 'foto4');
            $('#img-foto4').attr('style','display:none;')
        });
    $('.lbl-foto5').click(function(){
            $('#foto5').attr('name', 'foto5');
            $('#img-foto5   ').attr('style','display:none;')
        });
    function initImageUpload(box) {
      let uploadField = box.querySelector('.image-upload');

      uploadField.addEventListener('change', getFile);

      function getFile(e){
        let file = e.currentTarget.files[0];
        checkType(file);
      }
      
      function previewImage(file){
        let thumb = box.querySelector('.js--image-preview'),
            reader = new FileReader();

        reader.onload = function() {
          thumb.style.backgroundImage = 'url(' + reader.result + ')';
        }
        reader.readAsDataURL(file);
        thumb.className += ' js--no-default';
      }

      function checkType(file){
        let imageType = /image.*/;
        if (!file.type.match(imageType)) {
          throw 'Datei ist kein Bild';
        } else if (!file){
          throw 'Kein Bild gewählt';
        } else {
          previewImage(file);
        }
      }
      
    }

    // initialize box-scope
    var boxes = document.querySelectorAll('.box');

    for (let i = 0; i < boxes.length; i++) {
      let box = boxes[i];
      initDropEffect(box);
      initImageUpload(box);
    }



    /// drop-effect
    function initDropEffect(box){
      let area, drop, areaWidth, areaHeight, maxDistance, dropWidth, dropHeight, x, y;
      
      // get clickable area for drop effect
      area = box.querySelector('.js--image-preview');
      area.addEventListener('click', fireRipple);
      
      function fireRipple(e){
        area = e.currentTarget
        // create drop
        if(!drop){
          drop = document.createElement('span');
          drop.className = 'drop';
          this.appendChild(drop);
        }
        // reset animate class
        drop.className = 'drop';
        
        // calculate dimensions of area (longest side)
        areaWidth = getComputedStyle(this, null).getPropertyValue("width");
        areaHeight = getComputedStyle(this, null).getPropertyValue("height");
        maxDistance = Math.max(parseInt(areaWidth, 10), parseInt(areaHeight, 10));

        // set drop dimensions to fill area
        drop.style.width = maxDistance + 'px';
        drop.style.height = maxDistance + 'px';
        
        // calculate dimensions of drop
        dropWidth = getComputedStyle(this, null).getPropertyValue("width");
        dropHeight = getComputedStyle(this, null).getPropertyValue("height");
        
        // calculate relative coordinates of click
        // logic: click coordinates relative to page - parent's position relative to page - half of self height/width to make it controllable from the center
        x = e.pageX - this.offsetLeft - (parseInt(dropWidth, 10)/2);
        y = e.pageY - this.offsetTop - (parseInt(dropHeight, 10)/2) - 30;
        
        // position drop and animate
        drop.style.top = y + 'px';
        drop.style.left = x + 'px';
        drop.className += ' animate';
        e.stopPropagation();
        
      }
    }
</script>