<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Blog $blog
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Blog'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="blog form large-9 medium-8 columns content">
    <?= $this->Form->create($blog) ?>
    <fieldset>
        <legend><?= __('Add Blog') ?></legend>
        <?php
            echo $this->Form->control('user_id', ['options' => $users]);
            echo $this->Form->control('primer_titulo');
            echo $this->Form->control('segundo_titulo');
            echo $this->Form->control('introduccion');
            echo $this->Form->control('primer_texto');
            echo $this->Form->control('segundo_texto');
            echo $this->Form->control('url_foto1');
            echo $this->Form->control('url_foto2');
            echo $this->Form->control('url_foto3');
            echo $this->Form->control('url_foto4');
            echo $this->Form->control('url_foto5');
            echo $this->Form->control('foto1');
            echo $this->Form->control('foto2');
            echo $this->Form->control('foto3');
            echo $this->Form->control('foto4');
            echo $this->Form->control('foto5');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
