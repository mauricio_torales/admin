<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Ocategorias $categoria
 */
?>
<header class="page-header">
    <h2>Editar categoria</h2>   
</header>


<div class="row">
                            <div class="col-lg-12">
                                <section class="panel">
                                    <header class="panel-heading">
                                       
                        
                                        <h2 class="panel-title"></h2>
                                    </header>
                                    <div class="panel-body">

    <?= $this->Form->create($categoria, ['type' => 'file']) ?>


        <?php
             echo $this->Form->control('nombre_categoria', [
            'templates' => [
                'inputContainer' => '<div class="form-group">
                                                        <label class="col-sm-3 control-label" >Nombre de la categoria</label>
                                                        <div class="col-sm-6">{{content}}</div></div>'
            ],
            "class" => "form-control",
            "required",
            'label' => false
            ]);

           
        ?>


    <br>
                                            <center>
                                            <button type="Submit" class="mb-xs mt-xs mr-xs btn btn-default"><i class="fa  fa-save"></i> Guardar</button>
                                            </center>
    <?= $this->Form->end() ?>

</div>
