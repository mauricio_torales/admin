<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Categoria $categoria
 */
?>
<header class="page-header">
    <h2>Agregar categoria</h2>   
</header>
<div class="row">
                            <div class="col-lg-12">
                                <section class="panel">
                                    <header class="panel-heading">
                                       
                        
                                        <h2 class="panel-title"></h2>
                                    </header>
                                    <div class="panel-body">


    <?= $this->Form->create($categoria) ?>
  
                                        <div class="form-group">
                                                <label class="col-md-3 control-label" for="inputDefault">Nombre de la categoria</label>
                                                <div class="col-md-6">

                                                    <input type="text" class="form-control" name="nombre_categoria" id="nombre_categoria">

                                                </div>
                                            </div>
   
    <br>
                                            <center>
                                            <button type="Submit" class="mb-xs mt-xs mr-xs btn btn-default"><i class="fa  fa-save"></i> Guardar</button>
                                            </center>
    <?= $this->Form->end() ?>
</div>
