<?php 
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Categoria[]|\Cake\Collection\CollectionInterface $categorias
 */
?>
<header class="page-header">
    <h2>Categorias</h2> 
</header>
<div class="col-md-10">
        <section class="panel">
             <header class="panel-heading">
                   <div class="panel-actions">
                       <a href="#" class="fa fa-caret-down"></a>
                    </div>
                        <h2 class="panel-title">Lista de categorias</h2>
                             </header>
                                    <div class="panel-body">
                                        <div class="table-responsive">
                                            <table class="table mb-none">
                                                <thead>
                                                    <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Categoria') ?></th>
                <th scope="col" class="actions"><?= __('Eliminar') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($categorias as $categoria): ?>
            <tr>
                <td><?= $this->Number->format($categoria->id) ?></td>
                <td><?= h($categoria->nombre_categoria) ?></td>
                <td class="actions">
                   <?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-pencil')), ['action' => 'edit', $categoria->id], array('escape' => false)) ?>
                 
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
     <div class="paginator">
        <ul class="pagination">
          <?= $this->Paginator->first('<< ' . __('inicio')) ?>
          <?= $this->Paginator->prev('< ' . __('anterior')) ?>
           <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('siguiente') . ' >') ?>
         <?= $this->Paginator->last(__('ultimo') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Pagina {{page}} de {{pages}}, mostrando {{current}} registros de {{count}} en total')]) ?></p>
    </div>
</div>
