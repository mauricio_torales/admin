<section class="body"> 

			<!-- start: header -->
			<header class="header">
				<div class="logo-container">
					<a href="../" class="logo">
						<?php echo $this->Html->image('logo-negro.png', ['alt' => 'Logo', 'height'=> '35']); ?>
					</a>
					<div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
						<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
					</div>
				</div>
			<img src="" height="">
			
			</header>
			<!-- end: header -->
			<div class="inner-wrapper">
				<!-- start: sidebar -->
				<aside id="sidebar-left" class="sidebar-left">
				
					<div class="sidebar-header">
						
						<div class="sidebar-toggle hidden-xs" data-toggle-class="sidebar-left-collapsed" data-target="html" data-fire-event="sidebar-left-toggle">
							<i class="fa fa-bars" aria-label="Toggle sidebar"></i>
						</div>
					</div>
				
					<div class="nano">
						<div class="nano-content">
							<nav id="menu" class="nav-main" role="navigation">
								<ul class="nav nav-main">
									<li>
										<a href="javascript: void(0);">
											<i class="fa fa-home" aria-hidden="true"></i>
											<span>Inicio</span>
										</a>
									</li>
									
									<li class="nav-parent">
										<a>
											<i class="fa fa-shopping-cart" aria-hidden="true"></i>
											<span>Productos</span>
										</a>
										<ul class="nav nav-children">
											<li>
											<?php echo $this->Html->link('Listar productos', [
										          'controller' => 'Productos',
										          'action' => 'index',
										      ])
										      ; ?>
											</li>
											<li>
												<?php echo $this->Html->link('Agregar producto', [
										          'controller' => 'Productos',
										          'action' => 'add',
										      ])
										      ; ?>
											</li>
											<li class="nav-parent">
												<a>
													<i class="fa fa-bookmark-o" aria-hidden="true"></i>
													<span>Marcas</span>
												</a>
												<ul class="nav nav-children">
													<li>
													<?php echo $this->Html->link('Listar marcas', [
														'controller' => 'Marcas',
														'action' => 'index',
													])
													; ?>
													</li>
													<li>
														<?php echo $this->Html->link('Agregar marca', [
														'controller' => 'Marcas',
														'action' => 'add',
													])
													; ?>
											</li>
											
										</ul>
									</li>
									<li class="nav-parent">
										<a>
											<i class="fa fa-sitemap" aria-hidden="true"></i>
											<span>Categorias</span>
										</a>
										<ul class="nav nav-children">
											<li>
												<?php echo $this->Html->link('listar categorias', [
										          'controller' => 'Categorias',
										          'action' => 'index',
										      ])
										      ; ?>
											</li>
											<li>
												<?php echo $this->Html->link('Agregar categoria', [
										          'controller' => 'Categorias',
										          'action' => 'add',
										      ])
										      ; ?>
											</li>
											
										</ul>
									</li>
									<li class="nav-parent">
										<a>
											<i class="fa fa-tint" aria-hidden="true"></i>
											<span>Colores</span>
										</a>
										<ul class="nav nav-children">
											<li>
												<?php echo $this->Html->link('listar colores', [
										          'controller' => 'Colores',
										          'action' => 'index',
										      ])
										      ; ?>
											</li>
											<li>
												<?php echo $this->Html->link('Agregar color', [
										          'controller' => 'Colores',
										          'action' => 'add',
										      ])
										      ; ?>
											</li>
											
										</ul>
									</li>
									<li class="nav-parent">
										<a>
											<i class="fa fa-tag" aria-hidden="true"></i>
											<span>Talles</span>
										</a>
										<ul class="nav nav-children">
											<li>
												<?php echo $this->Html->link('listar tamaños', [
										          'controller' => 'Talles',
										          'action' => 'index',
										      ])
										      ; ?>
											</li>
											<li>
												<?php echo $this->Html->link('Agregar tamaño', [
										          'controller' => 'Talles',
										          'action' => 'add',
										      ])
										      ; ?>
											</li>
											
										</ul>
									</li>
											
										</ul>
									</li>
									<li class="nav-parent">
												<a>
													<i class="fa fa-file-picture-o" aria-hidden="true"></i>
													<span>Banners</span>
												</a>
												<ul class="nav nav-children">
													<li>
													<?php echo $this->Html->link('Listar banners', [
														'controller' => 'Banners',
														'action' => 'index',
													])
													; ?>
													</li>
													<li>
														<?php echo $this->Html->link('Agregar banner', [
														'controller' => 'Banners',
														'action' => 'add',
													])
													; ?>
													</li>
												</ul>
									</li>
									<li >
									<?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-book')).'Blog', array('controller' => 'blog', 'action' => 'edit','1'), array('escape' => false)) ?>
									</li>
									<li class="nav-parent">
												<a>
													<i class="fa fa-calendar" aria-hidden="true"></i>
													<span>Eventos</span>
												</a>
												<ul class="nav nav-children">
													<li>
													<?php echo $this->Html->link('Listar eventos', [
														'controller' => 'Eventos',
														'action' => 'index',
													])
													; ?>
													</li>
													<li>
														<?php echo $this->Html->link('Agregar evento', [
														'controller' => 'Eventos',
														'action' => 'add',
													])
													; ?>
													</li>
												</ul>
									</li>
									<li class="nav-parent">
												<a>
													<i class="fa fa-home" aria-hidden="true"></i>
													<span>Sucursales</span>
												</a>
												<ul class="nav nav-children">
													<li>
													<?php echo $this->Html->link('Listar sucursales', [
														'controller' => 'Sucursales',
														'action' => 'index',
													])
													; ?>
													</li>
													<li>
														<?php echo $this->Html->link('Agregar sucursal', [
														'controller' => 'Sucursales',
														'action' => 'add',
													])
													; ?>
													</li>
												</ul>
									</li>
									<li class="nav-parent">
												<a>
													<i class="fa fa-truck" aria-hidden="true"></i>
													<span>Distribuidores</span>
												</a>
												<ul class="nav nav-children">
													<li>
													<?php echo $this->Html->link('Listar distribuidores', [
														'controller' => 'Distribuidores',
														'action' => 'index',
													])
													; ?>
													</li>
													<li>
														<?php echo $this->Html->link('Agregar distribuidor', [
														'controller' => 'Distribuidores',
														'action' => 'add',
													])
													; ?>
													</li>
												</ul>
									</li>
									<li >
									<?php $id=$this->Session->read('Auth.User.id');?>
									<?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa fa-key')).'Cambiar contraseña', array('controller' => 'users', 'action' => 'edit',$id), array('escape' => false)) ?>

									</li>
									<li>
									<?= $this->Html->link($this->Html->tag('i', '', array('class' => 'fa  fa-sign-out')).'Salir', array('controller' => 'users', 'action' => 'logout'), array('escape' => false)) ?>

									</li>
									
									
								</ul>
							</nav>
							
						</div>
				
					</div>
				
				</aside>
				