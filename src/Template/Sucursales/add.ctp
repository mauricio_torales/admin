<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Sucursale $sucursale
 */
?>
<header class="page-header">
    <h2>Agregar sucursal</h2>   
</header>
<div class="row">
    <div class="col-lg-12">
        <section class="panel">
            <header class="panel-heading">
               <h2 class="panel-title"></h2></header>
               <div class="panel-body">
                    <?= $this->Form->create($sucursale) ?>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Nombre de la sucursal</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="nombre" id="nombre_categoria">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Telefono</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="telefono" id="nombre_categoria">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Horario de atención</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="horario" id="nombre_categoria">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Direccion</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="direccion" id="nombre_categoria">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" for="inputDefault">Localidad</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="localidad" id="nombre_categoria">
                            </div>
                        </div>
                        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    <strong>UBICACION</strong>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12">
                        <script type="text/javascript">
                            var map;
                            var positionCustomer;

                            $(document).ready(function(){
                              // se crea el mapa
                              map = new GMaps({
                                div: '#map',
                                lat: -25.287242,
                                lng: -57.634915,
                                zoom: 12
                              });

                              // ubicacion del customer
                              positionCustomer = map.addMarker({
                                lat: -25.287242,
                                lng: -57.634915,
                                draggable: true,
                                dragend: function(event) {
                                    var lat = event.latLng.lat();
                                    var lng = event.latLng.lng();
                                        var latlng = new google.maps.LatLng(lat, lng);
                                        positionCustomer.setPosition(latlng);
                                        $('.lat').val(lat)
                                        $('.lng').val(lng)

                                },
                              });

                              $('#geolocation-btn').click(function(event){
                                event.preventDefault();
                                GMaps.geolocate({
                                  success: function(position) {
                                    map.setCenter(position.coords.latitude, position.coords.longitude);
                                  var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
                                  $('.lat').val(position.coords.latitude)
                                  $('.lng').val(position.coords.longitude)
                                  positionCustomer.setPosition(latlng);
                                      
                                  },
                                  error: function(error) {
                                    alert('Geolocation failed: '+error.message);
                                  },
                                  not_supported: function() {
                                    alert("Your browser does not support geolocation");
                                  }
                                });
                              });
                              
                              $('#geolocation-btn').trigger('click');

                            });
                        </script>
                        <script>
                        var placeSearch, autocomplete;
                                var componentForm = {
                                    BillingNewAddress_Address1: 'short_name',
                                    BillingNewAddress_City: 'long_name'
                                };


                                function initAutocomplete() {
                                    // Create the autocomplete object, restricting the search to geographical
                                    // location types.
                                    autocomplete = new google.maps.places.Autocomplete(
                                        (document.getElementById('searchAddres')),
                                        {
                                            types: ['geocode'],
                                            componentRestrictions: { country: 'py' }
                                        });

                                    // When the user selects an address from the dropdown, populate the address
                                    // fields in the form.
                                    autocomplete.addListener('place_changed', fillInAddress);
                                }

                                function fillInAddress() {
                                    // Get the place details from the autocomplete object.
                                    var place = autocomplete.getPlace();
                                    // for (var component in componentForm) {
                                    //     document.getElementById(component).value = '';
                                    //     document.getElementById(component).disabled = false;
                                    // }

                                    map.setCenter(place.geometry.location.lat(), place.geometry.location.lng());
                                    var latlng = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
                                  positionCustomer.setPosition(latlng);
                                    //cargarInput(place.geometry.location.lat(), place.geometry.location.lng());
                                    $('.lat').val(place.geometry.location.lat())
                                    $('.lng').val(place.geometry.location.lng())
                                    // Get each component of the address from the place details
                                    // and fill the corresponding field on the form.
                                    for (var i = 0; i < place.address_components.length; i++) {
                                        var addressType = place.address_components[i].types[0];
                                        var val = place.address_components[i]["long_name"];
                                        if (addressType == "route") {
                                            $('#short_name').val(place.address_components[i]["long_name"]);
                                        }
                                        if (addressType == "locality") {
                                            $('#long_name').val(place.address_components[i]["long_name"]);
                                        }
                                    }
                                }


                        </script>

                        <script>
                          function addressToCoordenate(lat,lng){
                            var latlng = {lat: parseFloat(lat), lng: parseFloat(lng)};
                            var address = "https://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+","+lng+"&key=AIzaSyDOzs_Te0C0fhCfulT7JPWee2WvNr14ws0";
                            $.getJSON(address, function(response) {
                              if(response.status = "OK"){
                                for (var i = 0; i < response.results.length; i++) {
                                  var addressType = response.results[i].types;
                                  if (addressType == "route") {
                                    $('#short_name').val(response.results[i].address_components[0]["long_name"]);
                                  }
                                  if (addressType[0] == "locality") {
                                    $('#long_name').val(response.results[i].address_components[0]["long_name"]);
                                  }

                                }
                              }

                            });

                            }

                          

                        </script>
                          <div class="form-group">
                                <input class="form-control" name="search" id="searchAddres"  type="text" placeholder="Buscar" >
                            </div>
                          
                                              <div id="map" style="height: 400px;"></div>
                                              <br>
                                              <center>
                                                    <?php
                                                     echo $this->Form->control('latitud', [
                                                      'templates' => [
                                                          'inputContainer' => '<div class="form-group">
                                                                          <label class="col-sm-3 control-label" for="w4-username">Latitud</label>
                                                                          <div class="col-sm-9">{{content}}</div></div>'
                                                      ],
                                                      "class" => "form-control lat",
                                                      "",
                                                      'label' => false
                                                    ]);
                                                          echo $this->Form->control('longitud', [
                                                      'templates' => [
                                                          'inputContainer' => '<div class="form-group">
                                                                          <label class="col-sm-3 control-label" for="w4-username">Longitud</label>
                                                                          <div class="col-sm-9">{{content}}</div></div>'
                                                      ],
                                                      "class" => "form-control lng",
                                                      "",
                                                      'label' => false
                                                    ]); ?>
                                                      </center>
                                        
                        </div>
                    </div>
                </div>
            </div>
        </div>
   
    <br> <br>
                                            <center>
                                            <button type="Submit" class="mb-xs mt-xs mr-xs btn btn-default"><i class="fa  fa-save"></i> Guardar</button>
                                            </center>
    <?= $this->Form->end() ?>
</div>
