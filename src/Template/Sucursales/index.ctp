<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Sucursale[]|\Cake\Collection\CollectionInterface $sucursales
 */
?>
<header class="page-header">
    <h2> Sursales</h2>   
</header>
<section class="panel">
              <header class="panel-heading">
                <div class="panel-actions">

                  <button type="button" onclick="window.location.href='sucursales/add/'" class="mb-xs mt-xs mr-xs btn btn-success"><i class="fa fa-plus-circle"></i> Agregar sucursal</button>
                 
                </div>
            
                <h2 class="panel-title">Lista de sucursales</h2>
              </header>
              <div class="panel-body">
                <table class="table table-bordered table-striped mb-none" id="datatable-default">
                  <thead>
                    <tr>
                      <th>Id</th>
                      <th>Sucursal</th>
                      <th>Acciones</th>
                    </tr>
                  </thead>
                  <tbody>
            <?php foreach ($sucursales as $sucursal): ?>
            <tr class="gradeA">
                <td><?= $this->Number->format($sucursal->id) ?></td>
                <td><?= h($sucursal->nombre) ?></td>
                <td class="actions">
                                                            
                  <?= $this->Html->link($this->Html->tag('i', ' editar', array('class' => 'fa fa-pencil')), ['action' => 'edit', $sucursal->id], array('escape' => false)) ?>
                  <?= $this->Form->postLink($this->Html->tag('i', ' eliminar', array('class' => 'fa fa-trash-o')), ['action' => 'delete', $sucursal->id],array('escape' => false), ['confirm' => __('Esta seguro de eliminar este sucursal?', $sucursal->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
         </tbody>
                </table>
              
              </div>



</section>

