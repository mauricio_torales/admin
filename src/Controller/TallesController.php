<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Talles Controller
 *
 * @property \App\Model\Table\TallesTable $Talles
 *
 * @method \App\Model\Entity\Talle[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TallesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $talles = $this->paginate($this->Talles);

        $this->set(compact('talles'));
    }

    /**
     * View method
     *
     * @param string|null $id Talle id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $talle = $this->Talles->get($id, [
            'contain' => ['Productos']
        ]);

        $this->set('talle', $talle);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $talle = $this->Talles->newEntity();
        if ($this->request->is('post')) {
            $talle = $this->Talles->patchEntity($talle, $this->request->getData());
            if ($this->Talles->save($talle)) {
                $this->Flash->success(__('Talle agregada.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se pudo agregar, vuelva a intentarlo.'));
        }
        $productos = $this->Talles->Productos->find('list', ['limit' => 200]);
        $this->set(compact('talle', 'productos'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Talle id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $talle = $this->Talles->get($id, [
            'contain' => ['Productos']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $talle = $this->Talles->patchEntity($talle, $this->request->getData());
            if ($this->Talles->save($talle)) {
                $this->Flash->success(__('The talle has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The talle could not be saved. Please, try again.'));
        }
        $productos = $this->Talles->Productos->find('list', ['limit' => 200]);
        $this->set(compact('talle', 'productos'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Talle id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $talle = $this->Talles->get($id);
        if ($this->Talles->delete($talle)) {
            $this->Flash->success(__('se ha eliminado con exito.'));
        } else {
            $this->Flash->error(__('No se pudo eliminar, favor vuelva a intentarlo.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
