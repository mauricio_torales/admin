<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Colores Controller
 *
 * @property \App\Model\Table\ColoresTable $Colores
 *
 * @method \App\Model\Entity\Colore[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ColoresController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $colores = $this->paginate($this->Colores);

        $this->set(compact('colores'));
    }

    /**
     * View method
     *
     * @param string|null $id Colore id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $colore = $this->Colores->get($id, [
            'contain' => ['Productos']
        ]);

        $this->set('colore', $colore);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $colore = $this->Colores->newEntity();
        if ($this->request->is('post')) {
            $colore = $this->Colores->patchEntity($colore, $this->request->getData());
            if ($this->Colores->save($colore)) {
                $this->Flash->success(__('Color agregado'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se pudo guardar, favor vuelva a intentarlo.'));
        }
        $productos = $this->Colores->Productos->find('list', ['limit' => 200]);
        $this->set(compact('colore', 'productos'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Colore id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $colore = $this->Colores->get($id, [
            'contain' => ['Productos']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $colore = $this->Colores->patchEntity($colore, $this->request->getData());
            if ($this->Colores->save($colore)) {
                $this->Flash->success(__('The colore has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The colore could not be saved. Please, try again.'));
        }
        $productos = $this->Colores->Productos->find('list', ['limit' => 200]);
        $this->set(compact('colore', 'productos'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Colore id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $colore = $this->Colores->get($id);
        if ($this->Colores->delete($colore)) {
            $this->Flash->success(__('Color eliminado'));
        } else {
            $this->Flash->error(__('No se pudo eliminar, favor vuelva intentarlo'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
