<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Distribuidores Controller
 *
 * @property \App\Model\Table\DistribuidoresTable $Distribuidores
 *
 * @method \App\Model\Entity\Distribuidore[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DistribuidoresController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Users']
        ];
        $distribuidores = $this->paginate($this->Distribuidores);

        $this->set(compact('distribuidores'));
    }

    /**
     * View method
     *
     * @param string|null $id Distribuidore id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $distribuidore = $this->Distribuidores->get($id, [
            'contain' => ['Users']
        ]);

        $this->set('distribuidore', $distribuidore);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $distribuidore = $this->Distribuidores->newEntity();
        if ($this->request->is('post')) {
            $distribuidore = $this->Distribuidores->patchEntity($distribuidore, $this->request->getData());
            $distribuidore->user_id = $this->Auth->user('id');
            if ($this->Distribuidores->save($distribuidore)) {
                $this->Flash->success(__('The distribuidore has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The distribuidore could not be saved. Please, try again.'));
        }
        $users = $this->Distribuidores->Users->find('list', ['limit' => 200]);
        $this->set(compact('distribuidore', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Distribuidore id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $distribuidore = $this->Distribuidores->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $distribuidore = $this->Distribuidores->patchEntity($distribuidore, $this->request->getData());
            $distribuidore->user_id = $this->Auth->user('id');
            if ($this->Distribuidores->save($distribuidore)) {
                $this->Flash->success(__('The distribuidore has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The distribuidore could not be saved. Please, try again.'));
        }
        $users = $this->Distribuidores->Users->find('list', ['limit' => 200]);
        $this->set(compact('distribuidore', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Distribuidore id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $distribuidore = $this->Distribuidores->get($id);
        if ($this->Distribuidores->delete($distribuidore)) {
            $this->Flash->success(__('The distribuidore has been deleted.'));
        } else {
            $this->Flash->error(__('The distribuidore could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
