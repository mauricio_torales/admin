<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Productos Controller
 *
 * @property \App\Model\Table\ProductosTable $Productos
 *
 * @method \App\Model\Entity\Producto[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ProductosController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
  

    public function index()
    {
        $this->paginate = [
            'contain' => ['Users', 'Marcas', 'Categorias']
        ];
        $productos = $this->paginate($this->Productos);

        $this->set('productos', $this->Productos->find('all'));
    }

    /**
     * View method
     *
     * @param string|null $id Producto id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $producto = $this->Productos->get($id, [
            'contain' => ['Users', 'Marcas', 'Categorias', 'Colores', 'Talles']
        ]);

        $this->set('producto', $producto);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $producto = $this->Productos->newEntity();
        if ($this->request->is('post')) {
            $producto = $this->Productos->patchEntity($producto, $this->request->getData());
            $producto->user_id = $this->Auth->user('id');
            if ($this->Productos->save($producto)) {
                $this->Flash->success(__('El producto se ha guardado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('TNo se pudo guardar, favor vuelva a intentar'));
        }
        $users = $this->Productos->Users->find('list', ['limit' => 200]);
        $marcas = $this->Productos->Marcas->find('list',['keyField' => 'id','valueField' => 'nombre_marca'], ['limit' => 200]);
        $categorias = $this->Productos->Categorias->find('list',['keyField' => 'id','valueField' => 'nombre_categoria'],['limit' => 200]);

        $colores = $this->Productos->Colores->find('list',['keyField' => 'id','valueField' => 'color'], ['limit' => 200]);
        $talles = $this->Productos->Talles->find('list',['keyField' => 'id','valueField' => 'talle'],['limit' => 200]);
        $this->set(compact('producto', 'users', 'marcas', 'categorias', 'colores', 'talles'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Producto id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $producto = $this->Productos->get($id, [
            'contain' => ['Colores', 'Talles']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $producto = $this->Productos->patchEntity($producto, $this->request->getData());
            $producto->user_id = $this->Auth->user('id');
            if ($this->Productos->save($producto)) {
                $this->Flash->success(__('Se ha guardado los cambios'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('No se pudo actualizar, favor vuelva a intentar.'));
        }
         $users = $this->Productos->Users->find('list', ['limit' => 200]);
        $marcas = $this->Productos->Marcas->find('list',['keyField' => 'id','valueField' => 'nombre_marca'], ['limit' => 200]);
        $categorias = $this->Productos->Categorias->find('list',['keyField' => 'id','valueField' => 'nombre_categoria'],['limit' => 200]);

        $colores = $this->Productos->Colores->find('list',['keyField' => 'id','valueField' => 'color'], ['limit' => 200]);
        $talles = $this->Productos->Talles->find('list',['keyField' => 'id','valueField' => 'talle'],['limit' => 200]);
        $this->set(compact('producto', 'users', 'marcas', 'categorias', 'colores', 'talles'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Producto id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $producto = $this->Productos->get($id);
        if ($this->Productos->delete($producto)) {
            $this->Flash->success(__('Producto eliminado'));
        } else {
            $this->Flash->error(__('No se pudo eliminar, favor vuelva a intentar.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
